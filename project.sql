

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `year` int(11) NOT NULL,
  `price_min` int(11) NOT NULL,
  `price_max` int(11) NOT NULL,
  `miliege` int(11) NOT NULL,
  `cylinders` int(11) NOT NULL,
  `city_mpg` int(11) NOT NULL,
  `mpg` int(11) NOT NULL,
  `engine` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `name`, `year`, `price_min`, `price_max`, `miliege`, `cylinders`, `city_mpg`, `mpg`, `engine`) VALUES
(1, 'Ford focus', 2015, 50000, 1000000, 1, 4, 25, 30, 2),
(4, 'undefined', 0, 0, 0, 0, 0, 0, 0, 0),
(5, 'undefined', 0, 0, 0, 0, 0, 0, 0, 0),
(6, 'undefined', 0, 0, 0, 0, 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
