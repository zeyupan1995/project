var config 		= require('./config');
var mysql      	= require('mysql');
var express 	= require('express');
var app 		= express();
var bodyParser  = require('body-parser');

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 


//app.use(express.bodyParser());

var db = {

	host     : 'localhost',
	user     : 'root',
	password : '123qqq',
	database : 'project'

}

var connection = mysql.createConnection(db);
connection.connect();

var token = { token : '123qqq' };





//get all cars
app.get('/api/get', function (req, res) {

	auth(req,res);

	var data = mysql_getdata('cars','',res);

  	console.log('data to send', data);


});

//get one car
app.get('/api/get/:car_id', function(req, res){

	auth(req,res);

	var where = 'WHERE `id` = '+req.params.car_id;

	console.log(where);

	mysql_getdata('cars',where, res);

});

//add car
app.post('/api/car/add',function(req, res){

	auth(req,res);

	var name 		= req.query.name;
	var year 		= req.query.year;
	var price_min 	= req.query.price_min;
	var price_max 	= req.query.price_max;
	var miliege		= req.query.miliege;
	var cylinders	= req.query.cylinders;
	var city_mpg	= req.query.city_mpg;
	var mpg			= req.query.mpg;
	var engine 		= req.query.engine;

	var data = {
		name 	  : name,
		year 	  : year,
		price_min : price_min,
		price_max : price_max,
		miliege   : miliege,
		cylinders : cylinders,
		city_mpg  : city_mpg,
		mpg       : mpg,
		engine    : engine 
	};

	mysql_insertdata('cars',data,res);

});

//DELETE
app.delete('/api/car/delete/:id',function(req,res){

	auth(req,res);

	var id 		= req.params.id;
	var where	= 'WHERE `id` = '+id;

	console.log('deleting object id: ', id);

	mysql_deletedata('cars',where, res);
	
});


//update
app.put('/api/car/updata/:id',function(req,res){

	auth(req,res);

	var name 		= req.query.name;
	var year 		= req.query.year;
	var price_min 	= req.query.price_min;
	var price_max 	= req.query.price_max;
	var miliege		= req.query.miliege;
	var cylinders	= req.query.cylinders;
	var city_mpg	= req.query.city_mpg;
	var mpg			= req.query.mpg;
	var engine 		= req.query.engine;

	var data = {
		name 	  : name,
		year 	  : year,
		price_min : price_min,
		price_max : price_max,
		miliege   : miliege,
		cylinders : cylinders,
		city_mpg  : city_mpg,
		mpg       : mpg,
		engine    : engine 
	};

	mysql_updatedata('cars', data, req.params.id, res);


});

app.get('/headers',function(req,res){
	res.send( { code: 200, data: { headers: req.get('user-agent') } } );
});

//start server
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

function mysql_getdata(table,where,res)
{	

	if(!where) where = '';

	var sql = 'SELECT * FROM `'+table+'` '+ where;

	connection.query(sql, function(err, rows, fields) {
		if (err) throw err;

		console.log('sql is', sql);
		console.log('The solution is: ', rows);

		//return rows;
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.send({ code : 200, data : rows });
	});
}

function mysql_insertdata(table,data,res)
{
	var sql = 'INSERT INTO `'+table+'` (';

	Object.keys(data).forEach(function(key,i) {
		sql += ' `'+key+'` ';
		if(i < Object.keys(data).length - 1){
			sql += ",";
		}
	});

	sql += ') VALUES ( ';

	Object.keys(data).forEach(function(key,i){

		sql += '\'' + data[key] + '\'';
		if(i < Object.keys(data).length - 1){
			sql += ' , ';
		}

	});

	sql += ')';


	connection.query(sql, function(err,rows){
		if(err) throw err;

		console.log('sql is: ',sql);
		console.log('data is: ',rows);

		res.setHeader("Access-Control-Allow-Origin", "*");
		res.send({ code : 200, data : rows});
	});
	
}



function mysql_deletedata(table, where, res)
{

	var sql = 'DELETE FROM `' + table + '` '+where;

	connection.query(sql,function(err, rows){
		if(err) throw err;

		console.log('sql is: '+sql);
		console.log('data is: '+rows);

		res.setHeader("Access-Control-Allow-Origin", "*");
		res.send({ code : 200, data : rows });
	})
}

function mysql_updatedata(table, data, id, res)
{
    var sql = 'UPDATE `' + table + '` SET ';

    Object.keys(data).forEach(function(key,i,arr){

    	if(data[key]) sql += '`' + key + '` = \'' + data + '\'';
    	if(i < arr.length-1) sql += ' , ';

    });

    sql += " WHERE `id` = "+id;

	connection.query(sql,function(err, rows){
		if(err) throw err;

		console.log('sql is: '+sql);
		console.log('data is: '+rows);

		res.setHeader("Access-Control-Allow-Origin", "*");
		res.send({ code : 200, data : rows });
	})

}

function auth(req, res)
{	
	if (req.get('token') == token.token || req.query.token == token.token){
		
	}else{
		go();
	}

	function go(){
		var data = { code : 503, data : { errros : 'bad token' } };
		console.log(data);
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.send(data);
		throw '';
	}
}




